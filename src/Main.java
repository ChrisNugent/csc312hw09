import edu.princeton.cs.algs4.*;

public class Main {

    public static void main(String[] args) {
        final String[] fileNames = {"Bible_KJV.txt", "HoundOfTheBaskervilles.txt", "TheThreeMusketeers.txt",
                "TwentyThousandLeaguesUnderTheSea.txt"};
        for(String fileName : fileNames) {
            doFile(fileName);
            System.out.println("-------------------------------------------------------------------------------------");
        }
    }

    public static void doFile(String fileName) {
        Stopwatch stopwatch = new Stopwatch();
        In fileReader = new In(fileName);
        if (!fileReader.exists()) {
            System.out.println("Could not find " + fileName + ". Quitting...");
        }

        String fileContent = fileReader.readAll();
        fileReader.close();
        System.out.println("File read in " + stopwatch.elapsedTime() + "s");

        stopwatch = new Stopwatch();
        SuffixArrayX suffixArray = new SuffixArrayX(fileContent);
        System.out.println("SuffixArray built in " + stopwatch.elapsedTime() + "s");

        stopwatch = new Stopwatch();
        int lrsIndex = 0;
        int lrsLength = 0;
        for (int i = 1; i < fileContent.length(); i++) {
            int length = suffixArray.lcp(i);
            if (length > lrsLength) {
                lrsIndex = suffixArray.index(i);
                lrsLength = length;
            }
        }
        System.out.println("LRS found in " + stopwatch.elapsedTime() + "s");

        String lrs = fileContent.substring(lrsIndex, lrsIndex + lrsLength);

        System.out.println();
        System.out.println(fileName + " has " + fileContent.length() + " characters.");
        System.out.println("LRS has " + lrs.length() + " characters.");
        System.out.println("---LRS---");
        System.out.println(lrs);
    }
}